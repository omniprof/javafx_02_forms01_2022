package com.kenfogel.javafx_02_forms01_2022.presentation;

import com.kenfogel.javafx_02_forms01_2022.data.UserBean;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Example of a form created with code
 *
 * @author Ken
 */
public class Form01GUI {

    private final TextField userNameTextField;
    private final PasswordField userPasswordTextField;
    private final Text actionTarget;

    private final UserBean userBean;

    /**
     * Constructor that assigns the reference for the UserBean passed from the
     * MainApp and calls initialize to bind the bean to specific controls
     *
     * @param userBean
     */
    public Form01GUI(UserBean userBean) {
        this.userBean = userBean;
        userNameTextField = new TextField();
        userPasswordTextField = new PasswordField();
        actionTarget = new Text();
    }

    /**
     * The stage and the scene are created in the start.
     *
     * @param primaryStage
     */
    public void start(Stage primaryStage) {
        // Set Window's Title
        primaryStage.setTitle("JavaFX Form 01");
        Parent root = createUserInterface();
        root.setStyle("-fx-font: 24 arial;");
        Scene scene = new Scene(root, 500, 375);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Create the user interface as the root
     *
     * @return
     */
    private GridPane createUserInterface() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.BOLD, 36));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userName = new Label("User Name:");
        grid.add(userName, 0, 1);

        grid.add(userNameTextField, 1, 1);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 2);

        grid.add(userPasswordTextField, 1, 2);

        Button btn = new Button("Sign in");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 4);

        actionTarget.setId("actiontarget");
        grid.add(actionTarget, 0, 6, 2, 1);

        btn.setOnAction(this::signInButtonHandler);

        return grid;
    }

    /**
     * Event handler for the Sign In Button
     *
     * @param e
     */
    private void signInButtonHandler(ActionEvent e) {
        userBean.setUserName(userNameTextField.getText());
        userBean.setUserPassword(userPasswordTextField.getText());
        actionTarget.setText(userBean.toString());
    }
}
