package com.kenfogel.javafx_02_forms01_2022.data;

/**
 *
 * @author Ken Fogel
 */
public class UserBean {

    private String userName;
    private String userPassword;

    /**
     * Non-default constructor
     *
     * @param userName
     * @param userPassword
     */
    public UserBean(final String userName, final String userPassword) {
        this.userName = userName;
        this.userPassword = userPassword;
    }

    /**
     * Default Constructor
     */
    public UserBean() {
        this("", "");
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Override
    public String toString() {
        return "Name: " + userName + "  Password: " + userPassword;
    }

}
